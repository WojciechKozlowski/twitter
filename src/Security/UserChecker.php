<?php

namespace App\Security;

use App\Entity\User as AppUser;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements \Symfony\Component\Security\Core\User\UserCheckerInterface
{

    /**
     * @inheritDoc
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if ($user->getBannedUntil() === null) {
            return;
        }

        $date = $user->getBannedUntil();
        $now = new \DateTime('now');
        if ($date > $now) {
            $bannedDate = $date->format('Y-m-d H:i:s');
            throw new CustomUserMessageAccountStatusException("Your account is banned until $bannedDate ");
        }


    }

    /**
     * @inheritDoc
     */
    public function checkPostAuth(UserInterface $user): void
    {
    }
}