<?php

namespace App\DataFixtures;

use App\Entity\MicroPost;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    public function __construct(private UserPasswordHasherInterface $userPasswordHasher)
    {
    }

    public function load(ObjectManager $manager): void
    {

        $user = new User();
        $user->setEmail('wojtek@example.com');
        $user->setRoles(['ROLE_USER']);
        $user->setPassword($this->userPasswordHasher->hashPassword($user, '1234'));
        $manager->persist($user);

        $user2 = new User();
        $user2->setEmail('kasia@example.com');
        $user2->setRoles(['ROLE_USER']);
        $user2->setPassword($this->userPasswordHasher->hashPassword($user, '1234'));
        $manager->persist($user2);


        for ($i = 1; $i<11; $i++) {
            $microPost = new MicroPost();
            $microPost->setTitle("My  $i  post");
            $microPost->setText("Hello $i ");
            $microPost->setAuthor($user);
            $microPost->setCreated(new \DateTime());
            $manager->persist($microPost);
        }

        $manager->flush();
    }
}
