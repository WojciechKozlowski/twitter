<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\MicroPost;
use App\Form\CommentType;
use App\Form\MicroPostType;
use App\Repository\CommentRepository;
use App\Repository\MicroPostRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class MicroPostController extends AbstractController
{

    #[Route('/micro-post', name: 'app_micro_post')]
    public function index(MicroPostRepository $microPostRepository): Response
    {
        return $this->render('micro_post/index.html.twig', [
            'posts' => $microPostRepository->findAllWithComments()
        ]);
    }

    #[Route('/micro-post/{post}', name: 'app_micro_post_show')]
    public function showOne(MicroPost $post): Response
    {
        return $this->render('micro_post/show.html.twig', [
            'post' => $post
        ]);
    }

    #[IsGranted('IS_AUTHENTICATED_FULLY')]
    #[Route('/micro-post/add', name: 'app_micro_post_add', priority: 2)]
    public function add(Request $request, MicroPostRepository $microPostRepository): Response
    {

        if (!$this->getUser()->isVerified()) {
            throw $this->createAccessDeniedException('Access Denied.');
        }

        $form = $this->createForm(MicroPostType::class, new MicroPost());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setAuthor($this->getUser());
            $post->setCreated(new \DateTime());
            $microPostRepository->save($post, true);
            // Message to user after saving the post
            $this->addFlash(
                'success',
                'Your post were saved'
            );

            // Redirect user
            return $this->redirectToRoute('app_micro_post');
        }

        return $this->renderForm('micro_post/new.html.twig', [
            'form' => $form
        ]);
    }

    //todo make edit form to edit existing data


    #[Route('/micro-post/edit/{id}', name: 'app_micro_post_edit')]
    #[IsGranted(MicroPost::EDIT, 'id')]
    public function edit(Request $request, MicroPostRepository $microPostRepository, MicroPost $id): Response
    {
        $form = $this->createForm(MicroPostType::class, $id);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();
            $post->setAuthor($this->getUser());
            $post->setCreated(new \DateTime());
            $microPostRepository->save($post, true);
            // Message to user after saving the post
            $this->addFlash(
                'success',
                'Your post were updated'
            );

            // Redirect user
            return $this->redirectToRoute('app_micro_post');
        }

        return $this->renderForm('micro_post/edit.html.twig', [
            'form' => $form,
            'post' => $id
        ]);
    }

    #[Route('/micro-post/comment/{id}', name: 'app_micro_post_comment')]
    public function addComment(Request             $request,
                               CommentRepository   $commentRepository,
                                                   MicroPost $id): Response
    {

        $form = $this->createForm(CommentType::class, new Comment());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $comment->setAuthor($this->getUser());
            $comment->setCreated(new \DateTime());
            $comment->setPost($id);
            $commentRepository->save($comment, true);
            // Message to user after saving the post
            $this->addFlash(
                'success',
                'Your comment have been added'
            );

            return $this->redirectToRoute('app_micro_post_show',[
                'post' => $id->getId()
            ]);


        }

        return $this->renderForm('micro_post/comment.html.twig', [
            'form' => $form,
            'post' => $id
        ]);

    }


}

