<?php

namespace App\Command;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:create-user',
    description: 'This command will let you create a user',
)]
class CreateUserCommand extends Command
{

    private UserPasswordHasherInterface $userPasswordHasher;
    private UserRepository $userRepository;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher, UserRepository $userRepository)
    {
        parent::__construct();
        $this->userPasswordHasher = $userPasswordHasher;
        $this->userRepository = $userRepository;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('email', InputArgument::REQUIRED, 'Input user email')
            ->addArgument('password', InputArgument::REQUIRED, 'Input user password')
            ->addArgument('ROLE', InputArgument::OPTIONAL, 'role of the user')
            ->setHelp('This command allows you to create a user...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $password = $input->getArgument('password');
        $role = $input->getArgument('ROLE');

        $user = new User();

        $user->setEmail($email);
        $user->setPassword($this->userPasswordHasher->hashPassword($user,$password));
        if ($role) {
            $user->setRoles([$role]);
        }

        $this->userRepository->save($user, true);


        $io->success('The user has been created');

        return Command::SUCCESS;
    }
}
